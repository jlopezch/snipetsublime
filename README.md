# Configuracion Sublime text 3

##### Clonar el repositorio en la carpeta: **/home/user/.config/sublime-text3/Packages/User**

- Para mostrar carpetas ocultas en ubuntu (Ctrl + H)
- Para Mostar carpetas ocultas en Debian (Alt + .)

#### Pasos de configuracion:
```
1.- Istallar Package Control(*1)
2.- Instalar Material Theme y One Dark Material - Theme, (Ctrl + Shift + p, e ingresar la palabra:  install Package)
3.- Copiar el archivo Preferences.sublime-settings ubicado en la carpeta "snipetsublime" al directorio "User"(un directorio arriba)
4.- instalar los paquetes adicionales para mejorar sublime(Ctrl + Shift + p, e ingresar:  install Package):
- bootstrap 3 Snippets
- BracketHighlighter
- Color Highlighter
- DocBlockr
- Emmet
- GitGutter
- HTML Snippets
- JavaScript Completions
- javaScript Snippets
- jQuery
- jQuery Snippets pack
- Markdown Preview
- Material Theme
- One Dark Material - Theme
- Origami
- php-faker-completions
- php-snippets
- SublimeLinter
- SublimeLinter-php
- Yii2 Auto-complete
- Yii2 Snippets
```
***
#### Uso de snippets sublime:

**dumperD + tab:(Genera el siguiente codigo)** \yii\helpers\VarDumper::dump($log, 10, true);exit;

> Ver snippets disponibles: Ctrl + Shift + p, e ingresar la palabra: snippet

***
> Opciones Adicionales:

- [(*1) Instalar Package Control](https://packagecontrol.io/installation)
- [Atajos de teclado](http://falasco.org/sublime-text-2-cheat-sheet)
- [Manual EMMET](http://docs.emmet.io/cheat-sheet/)
- [Modificar preferencias Sublime](http://sublimetext.info/docs/es/reference/settings.html)
- [Informacio a detalle de cada paquete](https://packagecontrol.io/browse)
